Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f namespace.yaml
kubectl apply -n quotas-test -f quotas.yaml

kubectl get resourcequota ram-quotas -n=quotas-test --output=yaml
~~~~~~~~

Vous pouvez constater que l'utilisation actuelle du quota est de 0.

Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -n quotas-test -f deployment1.yaml

kubectl get resourcequota ram-quotas -n=quotas-test --output=yaml
~~~~~~~~

Vous pouvez constater que l'utilisation actuelle du quota tient compte du deployment.

Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -n quotas-test -f deployment2.yaml

kubectl get deploy -n quotas-test
kubectl get replicaset -n quotas-test
kubectl describe replicaset -l app=pinger2 -n quotas-test
~~~~~~~~

Vous pouvez constater que le deuxième deployment n'a pas abouti par manque de mémoire.

Exécutez la commande suivante :

~~~~~~~~
kubectl delete -n quotas-test -f .
~~~~~~~~