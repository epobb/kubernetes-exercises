Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f .

kubectl get pod -l app=gotty -o custom-columns=CONTAINER:.spec.serviceAccountName
~~~~~~~~

Le Pod gotty tourne avec l'identité *default*.

Affichez [http://localhost:8088](http://localhost:8088) dans le navigateur. Vous obtenez un terminal connecté au conteneur du pod *gotty*.

Exécutez la commande suivante dans le terminal du navigateur :

~~~~~~~~
curl https://kubernetes/api/v1/namespaces/default/secrets/ --insecure
~~~~~~~~

Vous obtenez une erreur *401 Unauthorized*.

Exécutez les commandes suivantes dans le terminal du navigateur :

~~~~~~~~
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/namespaces/default/secrets --insecure
~~~~~~~~

Vous obtenez une erreur *403 Forbidden*.

Fermez le navigateur. Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f final

kubectl get pod -l app=gotty -o custom-columns=CONTAINER:.spec.serviceAccountName
~~~~~~~~

Le Pod gotty tourne maintenant avec l'identité *gotty*.

Affichez [http://localhost:8088](http://localhost:8088) dans le navigateur. Vous obtenez un terminal connecté au conteneur du pod *gotty*.

Exécutez les commandes suivantes dans le terminal du navigateur :

~~~~~~~~
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/namespaces/default/secrets --insecure
~~~~~~~~

La liste des secrets est maintenant affichée.

Exécutez la commande suivante dans le terminal du navigateur :

~~~~~~~~
curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/namespaces/default/pods --insecure
~~~~~~~~

Vous obtenez une erreur *401 Unauthorized*. Cette opération n'a pas été autorisée dans le Role.

Exécutez la commande suivante :

~~~~~~~~
kubectl delete -f .
~~~~~~~~