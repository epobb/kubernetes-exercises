Le fichier deploy-adminer.yaml que vous allez utiliser a été créé avec la commande suivante :

~~~~~~~~
kubectl create deployment -o yaml pinger --dry-run=client --image=learnbook/k8s-pinger:0.6 > deploy-pinger.yaml
~~~~~~~~

Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f ./deploy-pinger.yaml

kubectl get deployments -l app=pinger
kubectl get pods -l app=pinger
~~~~~~~~
