Exécutez les commandes suivantes :

~~~~~~~~
kubectl delete pod -l app=pinger

kubectl get deployments -l app=pinger
kubectl get pods -l app=pinger

kubectl delete deployment pinger

kubectl get deployments -l app=pinger
kubectl get pods -l app=pinger

~~~~~~~~