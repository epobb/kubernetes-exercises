Exécutez les commandes suivantes :

~~~~~~~~
kubectl create deployment pinger --image=learnbook/k8s-pinger:0.6

kubectl get pods -l app=pinger

kubectl logs -l app=pinger
~~~~~~~~