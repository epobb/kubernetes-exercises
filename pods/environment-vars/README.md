Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f ./deploy-pinger.yaml

kubectl get deployments -l app=pinger
kubectl get pods -l app=pinger

kubectl logs -l app=pinger
~~~~~~~~
