Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f ./deploy-debugging.yaml

kubectl get deployments -l app=debugging
kubectl get pods -l app=debugging

kubectl describe pod -l app=debugging
~~~~~~~~

~~~~~~~~
kubectl apply -f ./deploy-debugging2.yaml

kubectl get deployments -l app=debugging2
kubectl get pods -l app=debugging2

kubectl describe pod -l app=debugging2
kubectl logs -l app=debugging2
~~~~~~~~
