Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f initial
~~~~~~~~

Affichez [http://localhost:8080/files](http://localhost:8080/files) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f deploy-front-files.yaml
~~~~~~~~

Affichez [http://localhost:8080/files](http://localhost:8080/files) dans le navigateur.

~~~~~~~~
kubectl apply -f deploy-front-envvar.yaml
~~~~~~~~

Affichez [http://localhost:8080/details](http://localhost:8080/details) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl delete -f initial
~~~~~~~~