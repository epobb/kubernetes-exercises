Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f initial
~~~~~~~~

Affichez [http://localhost:8080/files](http://localhost:8080/files) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f final
~~~~~~~~

Affichez [http://localhost:8080/files](http://localhost:8080/files) dans le navigateur.

Exécutez les commandes suivantes :

~~~~~~~~
kubectl delete -f final
kubectl apply -f final
~~~~~~~~

Affichez [http://localhost:8080/files](http://localhost:8080/files) dans le navigateur.