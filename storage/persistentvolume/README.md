Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f .
~~~~~~~~

Affichez [http://localhost:8080/files](http://localhost:8080/files) dans le navigateur.

Exécutez les commandes suivantes :

~~~~~~~~
kubectl delete -f deploy-front.yaml
kubectl delete -f service-front.yaml
kubectl apply -f .
~~~~~~~~

Affichez [http://localhost:8080/files](http://localhost:8080/files) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl delete -f .
~~~~~~~~