Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f ./deploy-back.yaml
kubectl apply -f ./service-back.yaml
~~~~~~~~

Affichez [http://localhost:8081/v1/currenttime](http://localhost:8081/v1/currenttime) dans le navigateur.

Exécutez les commandes suivantes :

~~~~~~~~
kubectl apply -f ./deploy-front.yaml
kubectl apply -f ./service-front.yaml
~~~~~~~~

Affichez [http://localhost:8080](http://localhost:8080) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f ./service-back-inside.yaml
~~~~~~~~

Affichez [http://localhost:8080](http://localhost:8080) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl delete -f .
~~~~~~~~
