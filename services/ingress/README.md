Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f .
~~~~~~~~

Affichez [http://front.me](http://front.me]) et [http://back.me](http://back.me]) dans le navigateur.

Exécutez les commandes suivantes (Docker desktop) :

~~~~~~~~
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install default-ingress ingress-nginx/ingress-nginx
~~~~~~~~

Exécutez la commande suivante (Minikube) :

~~~~~~~~
minikube addons enable ingress
~~~~~~~~

Affichez [http://front.me](http://front.me]) et [http://back.me](http://back.me]) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl delete -f .
~~~~~~~~
