Exécutez la commande suivante :

~~~~~~~~
kubectl apply -f .
~~~~~~~~

Affichez [http://localhost:8080](http://localhost:8080) dans le navigateur.

Exécutez la commande suivante :

~~~~~~~~
kubectl delete -f .
~~~~~~~~